import FWCore.ParameterSet.Config as cms

process = cms.Process("EXERCISE1")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1000) )

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
        'file:/pnfs/desy.de/cms/tier2/store/mc/RunIISpring16MiniAODv2/GluGluToRadionToHHTo2B2Tau_M-300_narrow_13TeV-madgraph/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14_ext1-v1/80000/02EE1330-5D3B-E611-B5AD-001CC4A7C0A4.root'
    )
)

process.myMuonCollectionWithIsolation = cms.EDProducer('MyMuonIsolationProducer',
    muons = cms.InputTag('slimmedMuons')
)

process.out = cms.OutputModule("PoolOutputModule",
    fileName = cms.untracked.string('myMuonIsolationOutputFile.root')
)

  
process.p = cms.Path(process.myMuonCollectionWithIsolation)

process.e = cms.EndPath(process.out)
