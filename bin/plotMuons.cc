#include <string>
#include <vector>

#include <TH1F.h>
#include <TROOT.h>
#include <TFile.h>
#include <TSystem.h>

#include "DataFormats/FWLite/interface/Event.h"
#include "DataFormats/Common/interface/Handle.h"
#include "FWCore/FWLite/interface/FWLiteEnabler.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "PhysicsTools/FWLite/interface/TFileService.h"
#include "PhysicsTools/FWLite/interface/CommandLineParser.h"


int main(int argc, char* argv[]) 
{
  using pat::Muon;
  gSystem->Load( "libFWCoreFWLite" );
  FWLiteEnabler::enable();

  // initialize command line parser, set defaults and read in command line arguments
  optutl::CommandLineParser parser ("Analyze FWLite Histograms");
  parser.stringValue  ("outputFile" ) = "analyzeMuons.root";
  parser.parseArguments (argc, argv);
  std::string outputFile_ = parser.stringValue("outputFile");
  std::vector<std::string> inputFiles_ = parser.stringVector("inputFiles");

  // book a set of histograms
  fwlite::TFileService fs = fwlite::TFileService(outputFile_.c_str());
  TFileDirectory dir = fs.mkdir("analyzeMuons");
  TH1F* muonIso_ = dir.make<TH1F>("muonIso" , "muon isolation;isolation [GeV];number of muons" , 100 ,  0. ,  10.);

  // loop the events
  int ievt=0;  
  for(unsigned int iFile=0; iFile<inputFiles_.size(); ++iFile){
    TFile* inFile = TFile::Open(inputFiles_[iFile].c_str());
    if( inFile ){
      fwlite::Event ev(inFile);
      for(ev.toBegin(); !ev.atEnd(); ++ev, ++ievt){
	edm::EventBase const & event = ev;
	edm::Handle<std::vector<Muon> > muons;
	event.getByLabel(std::string("myMuonCollectionWithIsolation"), muons);
	
	// loop muon collection and fill histograms
	for(std::vector<Muon>::const_iterator mu=muons->begin(); mu!=muons->end(); ++mu){
          muonIso_->Fill( mu->userIsolation(pat::IsolationKeys(7)) );
	}
      }  
      // close input file
      inFile->Close();
    }
  }
  return 0;
}
