#include <memory>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>

#include <TH1F.h>
#include <TROOT.h>
#include <TFile.h>
#include <TSystem.h>

#include "DataFormats/FWLite/interface/Event.h"
#include "DataFormats/Common/interface/Handle.h"
#include "FWCore/FWLite/interface/FWLiteEnabler.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "PhysicsTools/FWLite/interface/TFileService.h"
#include "PhysicsTools/FWLite/interface/CommandLineParser.h"


int main(int argc, char* argv[]) 
{
  using pat::Muon;
  gSystem->Load( "libFWCoreFWLite" );
  FWLiteEnabler::enable();

  // initialize command line parser, set defaults and read in command line arguments
  optutl::CommandLineParser parser ("Analyze FWLite Histograms");
  parser.integerValue ("maxEvents"  ) = 1000;
  parser.stringValue  ("outputFile" ) = "analyzeMuons.root";
  parser.parseArguments (argc, argv);
  int maxEvents_ = parser.integerValue("maxEvents");
  std::string outputFile_ = parser.stringValue("outputFile");
  std::vector<std::string> inputFiles_ = parser.stringVector("inputFiles");

  // book a set of histograms
  fwlite::TFileService fs = fwlite::TFileService(outputFile_.c_str());
  TFileDirectory dir = fs.mkdir("analyzeMuons");
  TH1F* muonPt_  = dir.make<TH1F>("muonPt"  , "pt"        , 100 ,  0. , 300.);
  TH1F* muonEta_ = dir.make<TH1F>("muonEta" , "eta"       , 100 , -3. ,   3.);
  TH1F* muonPhi_ = dir.make<TH1F>("muonPhi" , "phi"       , 100 , -5. ,   5.);  
  TH1F* muonIso_ = dir.make<TH1F>("muonIso" , "isolation" , 100 ,  0. ,  10.);

  // loop the events
  int ievt=0;  
  for(unsigned int iFile=0; iFile<inputFiles_.size(); ++iFile){
    // open input file (can be located on castor)
    TFile* inFile = TFile::Open(inputFiles_[iFile].c_str());
    if( inFile ){
      fwlite::Event ev(inFile);
      for(ev.toBegin(); !ev.atEnd(); ++ev, ++ievt){
	edm::EventBase const & event = ev;
	// simple event counter
	if(outputEvery_!=0 ? (ievt>0 && ievt%outputEvery_==0) : false) 
	  std::cout << "  processing event: " << ievt << std::endl;

	edm::Handle<std::vector<Muon> > muons;
	event.getByLabel(std::string("myMuonCollectionWithIsolation"), muons);
	
	// loop muon collection and fill histograms
	for(std::vector<Muon>::const_iterator mu=muons->begin(); mu!=muons->end(); ++mu){
	  muonPt_ ->Fill( mu->pt () );
	  muonEta_->Fill( mu->eta() );
	  muonPhi_->Fill( mu->phi() );	  
          muonIso_->Fill( mu->userIsolation(pat::IsolationKeys(7)) );
	}
      }  
      // close input file
      inFile->Close();
    }
    // break loop if maximal number of events is reached:
    // this has to be done twice to stop the file loop as well
    if(maxEvents_>0 ? ievt+1>maxEvents_ : false) break;
  }
  return 0;
}
