// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/stream/EDProducer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/StreamID.h"

#include "DataFormats/PatCandidates/interface/Muon.h"

//
// class declaration
//

class MyMuonIsolationProducer : public edm::stream::EDProducer<> {
   public:
      explicit MyMuonIsolationProducer(const edm::ParameterSet&);
      ~MyMuonIsolationProducer();

      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

   private:
      virtual void beginStream(edm::StreamID) override;
      virtual void produce(edm::Event&, const edm::EventSetup&) override;
      virtual void endStream() override;

      edm::EDGetTokenT<pat::MuonCollection> muonsToken_;
};

//
// constructors and destructor
//
MyMuonIsolationProducer::MyMuonIsolationProducer(const edm::ParameterSet& iConfig)
{
  produces<pat::MuonCollection>();
  muonsToken_ = consumes<pat::MuonCollection>(iConfig.getParameter<edm::InputTag>("muons"));
}


MyMuonIsolationProducer::~MyMuonIsolationProducer()
{
}


//
// member functions
//

// ------------ method called to produce the data  ------------
void
MyMuonIsolationProducer::produce(edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  edm::Handle<pat::MuonCollection> muons;
  iEvent.getByToken(muonsToken_,muons);
  std::unique_ptr<pat::MuonCollection> muonsCopy(new pat::MuonCollection(*muons)); 

  for(std::vector<pat::Muon>::iterator mu=muonsCopy->begin(); mu!=muonsCopy->end(); ++mu){
    float isolation = (mu->pfIsolationR04().sumChargedHadronPt + std::max(0., mu->pfIsolationR04().sumNeutralHadronEt + mu->pfIsolationR04().sumPhotonEt - 0.5*mu->pfIsolationR04().sumPUPt))/mu->pt();
    mu->setIsolation(pat::IsolationKeys(7),isolation);
  }

  iEvent.put(std::move(muonsCopy));
}

// ------------ method called once each stream before processing any runs, lumis or events  ------------
void
MyMuonIsolationProducer::beginStream(edm::StreamID)
{
}

// ------------ method called once each stream after processing all runs, lumis and events  ------------
void
MyMuonIsolationProducer::endStream() {
}

void
MyMuonIsolationProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>("muons")->setComment("input muon collection");
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(MyMuonIsolationProducer);
